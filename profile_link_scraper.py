#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup


# >>> Requests
base_url = "https://profiles.uonbi.ac.ke/"
# f_page = "0"  # jQuery frontpage on base_url's DOM

f_page = list(range(1, 1087))
# returns list of integers between 1 and n


for i in range(len(f_page)):

    query_parameter = "?page={0}".format(str(f_page[i]))
    # converts int  from f_page to str and concatenates to ?page=

    url = base_url + query_parameter
    page = requests.get(url)

    soup = BeautifulSoup(page.text, 'html.parser')  # parser
    # print(soup)

    tb = soup.find('tbody')  # front page table body
    # print(tb)

    for tr in tb.find_all('tr'):
        for td in tr.find_all('td'):
            for div in td.find_all('div', {'class': 'views-field-title'}):
                # print(div)
                for span in div.find_all('span', {'class': 'field-content'}):
                    # print(span)
                    for link in span.find_all('a'):
                        profile_link = (link.get('href'))
                        print(profile_link)

                        '''
                        with open('profile-links.txt', 'w') as f:
                            f.write(profile_link + '\n')
                        '''
