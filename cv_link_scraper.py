#!/usr/bin/env python3
"""
this script should be run from the result of every link returned in profiles.py and get download links to the CVs
"""
import requests
import json
from bs4 import BeautifulSoup
import os

output_file = "linkfiles/dict_plus_links.txt"
os.makedirs(os.path.dirname(output_file), exist_ok=True)
error_file = "linkfiles/staff_missing_doc.txt"

i = 0
error_pages = []
cvDict = {}

with open("profile-links.txt", mode="r") as file:
    lines = file.readlines()


# define function to get document names
for line in lines:
    try:
        url = line.strip()
        page = requests.get(url)
        soup = BeautifulSoup(page.text, "html.parser")
        # print(soup.prettify())

        # follows simplifying DOM
        top_header = soup.find('div', {'id': 'page'})
        hg_cont = top_header.find('div', {'class': 'hg-container'})
        main_header = hg_cont.find('div', {'id': 'header-main'})
        block_boxes = main_header.find('div', {
            'class': 'box-os_boxes_modal_siteinfo block region-odd odd region-count-1 count-3'})
        mh_content = block_boxes.find('div', {'class': 'content'})
        boxes_box = mh_content.find('div', {'class': 'boxes-box'})
        boxes_box_content = boxes_box.find(
            'div', {'class': 'boxes-box-content'})
        h1_name = boxes_box_content.find('h1')
        file_name = h1_name.text

    except AttributeError:
        pass


# define function to get document links
    try:
        ddl_link = soup.find('h3', {'class', 'cv-direct-download'})
        link = ddl_link.find('a')
        download_link = (link.get('href'))
        cvDict[file_name] = download_link

    except AttributeError:
        # print("Error fetching CV for {0}" .format(file_name))
        error_pages.append(file_name)

    # error links (to be added to the error message)
        '''
        for link in h1_name.find_all('a'):
        err_link = (link.get('href'))
        # print(err_link)

        '''

    # output file
with open(output_file, 'w') as out_f:
    json.dump(cvDict, out_f)

with open(error_file, 'w') as erronous_file:
    erronous_file.write("Error fetching CVs for: \n\n")
    for staff in error_pages:
        erronous_file.write(f"{staff}\n")
