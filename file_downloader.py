#!/usr/bin/env python3
"""
This script downloads and stores the files into its own directory to compress later
"""

from urllib.request import urlopen
import json
import os

json_file = 'linkfiles/dict_plus_links.txt'
with open(json_file, 'r') as file:
    cvDict = json.load(file)

cvDirectory = 'downloaded_CVs'
try:
    os.mkdir(cvDirectory)
except FileExistsError:
    pass

for key, value in cvDict.items():
    url = value
    save_as = f"{cvDirectory}/{key}.pdf"

    # Download from URL
    with urlopen(url) as file:
        content = file.read()

    # Save to file
    with open(save_as, 'wb') as download:
        download.write(content)
