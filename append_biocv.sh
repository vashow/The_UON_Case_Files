#!/usr/bin/env bash
#Appends biocv to all lines of the provided text file

sed -i 's/$/biocv/g' "$1"
