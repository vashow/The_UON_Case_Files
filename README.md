# The Case Files
Herein lies the scripts used to download and compress every single cv from all the profiles listed in https://profiles.uonbi.ac.ke
Most of the script is executed with python utilising BeautifulSoup and requests.
### profile_link_scraper.py
This script scrapes every single link to individual profiles of the UoN staff, by navigating down the html tags.

### append_biocv.sh
After getting a file containing links to individual staff pages we pass it to the this bash script which then appends the 'biocv' to it so as to get pages specifically containing the cv of the person of interest.

### cv_link_scraper.py
After processing the links this script then goes through biocv pages of individual staff and extracts the download links to the cvs and creates a a list of dictionaries with the keys being a string containing the name of the staff and the value being the link.
This was done so as to be able to label the downloaded files to the name of the individual.
The dictionaries are then dumped into a specified json file in the created directory 'linkfiles' for now the final downloading of CVs
The pages where no cv was found are documented in 'linkfiles/staff_missing_doc.txt'

### file_downloader.py
Utilising the json file, 'linkfiles/dict_plus_links.txt' this script downloads the cvs in pdf form, renames them to the staffs title and stores them into the directory 'downloaded_CVs' thus bring a close to the scraping of the sites CVs

#### Additional info
The complete list of links to the staff biocvs is in 'biocvLinks.txt' while 'profile-links.txt' is its subset for the production process though eventually when the whole script is run we will utilise the data in 'biocvLinks.txt' as a whole.

##### Credits
###### The Morans
+ Otweezy (MBUZI) - The web and python wizard with a sleep-cycle of a mad man
+ Vashow (The Laibon) - The mad-creative hell-bent on bash and low-level
